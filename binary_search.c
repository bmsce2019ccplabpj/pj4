#include <stdio.h>
int main()
{
    int n,a[20],key,pos=0,i,beg,mid,end;
    printf("how many elements you want to enter in the array?");
    scanf("%d",&n);
    printf("Enter elements into the array:");
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
    printf("Input Array::\n");
    for(i=0;i<n;i++)
    {
        printf("%d\t",a[i]);
    }
    printf("\n");
    beg=0;
    end=n-1;
    printf("Enter the element to be searched:");
    scanf("%d",&key);
    while(beg<=end)
    {
            mid=(beg+end)/2;
            if(a[mid]==key)
            {
                pos=mid;
                break;
            }
            if(a[mid]<key)
            {
               beg=mid+1;
            }
            else if(a[mid]>key)
            {
                end=mid-1;
            }
           
    }
    
    pos=pos+1;
    printf("Element %d found at %d position\n",key,pos);
    return 0;
}
        