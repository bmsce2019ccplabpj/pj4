#include <stdio.h>
int input(int n)
{
    printf("Enter any number:");
    scanf("%d",&n);
    return n;
}
int rev_num(int n)
{
    int rev=0,r;
    while(n!=0)
    {
        r=n%10;
        rev=rev*10+r;
        n=n/10;
    }
    printf("The number after reversing=%d\n",rev);
    return rev;
}
void check_palindrome(int n,int t)
{
    if(t==n)
        printf("%d is a palindrome\n",t);
    else
        printf("%d is not a palindrome\n",t);
        
}
int main()
{
    int num=0,rev,temp;
    num=input(num);
    temp=num;
    rev=rev_num(num);
    check_palindrome(rev,temp);
    return 0;
}