#include<stdio.h>
void input(int *a,int *b)
{
    printf("Enter a number::");
    scanf("%d",&*a);
    printf("Enter another number::");
    scanf("%d",&*b);
}
void swap(int *a,int *b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
void print(int *a,int *b)
{
     printf("a=%d\tb=%d\n",*a,*b);
}
int main()
{
    int a,b;
    input(&a,&b);
    printf("numbers before swapping::\n");
    print(&a,&b);
    swap(&a,&b);
    printf("numbers after swapping::\n");
    print(&a,&b);
    return 0;
    
}

    