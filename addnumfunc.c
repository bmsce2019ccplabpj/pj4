#include<stdio.h>
void input(int *a,int *b)
{
    printf("Enter 2 numbers:");
    scanf("%d %d",&*a,&*b);
}
void sum(int *a,int *b,int *s)
{
    *s=*a+*b;
}
void print(int *s)
{
    printf("sum is %d\n",*s);
}
int main()
{
    int a,b,s;
    input(&a,&b);
    sum(&a,&b,&s);
    print(&s);
    return 0;
}